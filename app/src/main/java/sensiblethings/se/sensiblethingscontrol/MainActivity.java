package sensiblethings.se.sensiblethingscontrol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import se.sensiblethings.disseminationlayer.communication.Communication;
import se.sensiblethings.disseminationlayer.communication.rudp.RUDPCommunication;
import se.sensiblethings.disseminationlayer.lookupservice.LookupService;
import se.sensiblethings.disseminationlayer.lookupservice.kelips.KelipsLookup;
import se.sensiblethings.interfacelayer.SensibleThingsListener;
import se.sensiblethings.interfacelayer.SensibleThingsNode;
import se.sensiblethings.interfacelayer.SensibleThingsPlatform;

// Example deliberately ignores aync
public class MainActivity extends AppCompatActivity implements SensibleThingsListener {
    SensibleThingsPlatform platform = null;

    String sinkUci = "fantasticfears@gmail.com/sink";
    String operationUci = "fantasticfears@gmail.com/operation";

    String androidUci = "fantasticfears@gmail.com/android";
    String controlForPotentiometerUci = "fantasticfears@gmail.com/android/potentiometer";
    String controlForLEDUci = "fantasticfears@gmail.com/android/led";
    String controlForServoUci = "fantasticfears@gmail.com/android/servo";
    String commandOnPi = "fantasticfears@gmail.com/pi/command";

    String ledOnArduino = "fantasticfears@gmail.com/arduino/led";
    String servoOnArduino = "fantasticfears@gmail.com/arduino/servo";
    String potentiometerOnArduino = "fantasticfears@gmail.com/arduino/potentiometer";

    boolean runBootstrap = false;
    Integer bindingPortNumber = 9009;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Switch toggle = (Switch) findViewById(R.id.servoSwitch);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                if (platform != null) {
                    platform.resolve(commandOnPi);
                    platform.resolve(operationUci);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggle.setChecked(!isChecked);
                        }
                    });
                    startPlatform();
                }
            }
        });

        Button button= (Button) findViewById(R.id.sendButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (platform != null) {
                    platform.resolve(sinkUci);
                } else {
                    startPlatform();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Create the platform itself
        if (platform == null) {
            startPlatform();
        }
    }


    @Override
    protected void onPause() {
        //If we leave application, shutdown MediaSense as well
        if(platform != null){
            platform.shutdown();
            platform = null;
        }
        finish(); //and shutdown the app
        super.onPause();
    }

    @Override
    public void resolveResponse(final String uci, final SensibleThingsNode node) {
        if (uci.equals(sinkUci)) { // set for text
            final EditText editMessage = (EditText) findViewById(R.id.editMessage);
            platform.set(androidUci, editMessage.getText().toString(), node);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    editMessage.setText("");
                }
            });

        } else if (uci.equals(operationUci)) { // uploads operations
            Switch toggle = (Switch) findViewById(R.id.servoSwitch);
            platform.set(androidUci, toggle.isChecked() ? "true" : "false", node);
        } else if (uci.equals(commandOnPi)) { // sending messages to pi
            Switch toggle = (Switch) findViewById(R.id.servoSwitch);
            platform.set(androidUci, toggle.isChecked() ? "true" : "false", node);
        }
    }

    @Override
    public void getResponse(final String uci, final String value, final SensibleThingsNode source) {
        if (uci.equals(servoOnArduino)) {
            final String resolvedValue = value;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView servoStatus = (TextView) findViewById(R.id.servoStatus);
                    servoStatus.setText("Servo position: " + resolvedValue);
                }
            });
        } else if (uci.equals(potentiometerOnArduino)) {
            final String resolvedValue = value;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView potentiometerStatus = (TextView) findViewById(R.id.potentiometerStatus);
                    potentiometerStatus.setText("Potentiometer: " + resolvedValue);
                }
            });
        }
    }

    @Override
    public void getEvent(SensibleThingsNode source, String uci) {
        //Not used in this example
    }

    @Override
    public void setEvent(SensibleThingsNode source, String uci, String value) {
        if (uci.equals(servoOnArduino)) {
            final String resolvedValue = value;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView servoStatus = (TextView) findViewById(R.id.servoStatus);
                    servoStatus.setText("Servo: " + resolvedValue);
                }
            });
        } else if (uci.equals(potentiometerOnArduino)) {
            final String resolvedValue = value;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView potentiometerStatus = (TextView) findViewById(R.id.potentiometerStatus);
                    potentiometerStatus.setText("Potentiometer: " + resolvedValue);
                }
            });
        }
    }

    private void startPlatform() {
        Thread initThread = new Thread(new Runnable() {
            @Override
            public void run() {
                RUDPCommunication.initCommunicationPort = bindingPortNumber;

                KelipsLookup.BOOTSTRAP = runBootstrap;
                KelipsLookup.bootstrapIp = "192.168.1.2";

                Log.e("SensibleThings", "platform prepared");
                platform = new SensibleThingsPlatform(
                        LookupService.KELIPS,
                        Communication.RUDP,
                        MainActivity.this);
                Log.e("SensibleThings", "platform init");
                platform.register(controlForPotentiometerUci);
                platform.register(controlForServoUci);
                platform.register(controlForLEDUci);
                Log.e("SensibleThings", "ready on " + (bindingPortNumber));
                platform.resolve(operationUci);
            }
        });
        initThread.start();
    }
}
